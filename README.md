# catsvc

Example Flask web service that returns [kaomoji](https://en.wikipedia.org/wiki/Kaomoji) images of cats. 

Used for demonstration of DevOps/SRE practices.

Example:

```
$ curl localhost:5000
₍^.  ̫.^₎
```

## How to Run

The webservice will listen on port 5000 by default.

```
pip install --no-cache-dir --upgrade -r requirements.txt
gunicorn app:app
```

There is also a pre-built [Docker image](https://hub.docker.com/r/certomodo/catsvc) available.

## Configuration Options

### Gunicorn Settings
CatSvc uses Gunicorn as its WSGI HTTP server. You can override default settings via the `GUNICORN_CMD_ARGS` environment variable.

For example, the service listens on port 5000/tcp by default. To change it to port 12345, use `--bind`:

`GUNICORN_CMD_ARGS="--bind :12345"`

Consult the [Gunicorn website](https://docs.gunicorn.org/en/stable/settings.html) for the complete list.

### catsvc Settings
These environment variables change the performance and reliability of this service, which is useful in simulating production incidents and SLO violations.

#### Latency
Environment variable `FLASK_LATENCY_MAX` will introduce a random amount of latency (in milliseconds) on a per-request basis up to that maximum using an exponential distribution.

`FLASK_LATENCY_MAX` must be a positive integer.

#### Error Rate
Environment variable `FLASK_ERRRATE` introduces a probability of generating a 500 error for a given request.

`FLASK_ERRRATE` must be a float in the range between [0,1].
