from flask import Flask, abort
import random
import os
import time

app = Flask(__name__)

cats = [
    "≽^•⩊•^≼",
    "₍^.  ̫.^₎",
    "ᓚ₍ ^. .^₎",
    "•⩊•",
    "₍^.  ̫.^₎",
    "ฅ^•ﻌ•^ฅ",
    "/ᐠ - ˕ -マ Ⳋ",
    "₍^ >ヮ<^₎ .ᐟ.ᐟ",
    "ฅ/ᐠ˶> ﻌ<˶ᐟ\ฅ",
    "( • ̀ω•́ )✧",
]


# Sleep for a random latency between 0 and end using exponential distribution.
def random_latency(end):
    mean = end / 2
    lambda_param = 1 / mean
    while True:
        random_value = random.expovariate(lambda_param)
        if random_value <= end:
            break
    time.sleep(random_value / 1000)


# Throw an HTTP 500 with probability errrate.
def random_failure(errrate):
    if random.randrange(0, int(1 / float(errrate))) == 0:
        abort(500, description="Internal Server Error(Simulated)")


@app.errorhandler(500)
def internal_server_error(e):
    return str(e.description), 500


@app.route("/")
def get_cat():
    app.config.from_prefixed_env()
    if "LATENCY_MAX" in app.config:
        # Decide how much artificial latency to add
        random_latency(app.config["LATENCY_MAX"])

    if "ERRRATE" in app.config:
        # Decide whether to throw an error
        errrate = app.config["ERRRATE"]
        random_failure(errrate)

    # Finally, print random cat ^_^
    return random.choice(cats) + "\n"
