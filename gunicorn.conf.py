import multiprocessing

max_requests = 1000
max_requests_jitter = 50
log_file = "-"
workers = 1
bind = "0.0.0.0:5000"
accesslog = "-"
# Adds %M which is request duration in ms
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s %(M)s "%(f)s" "%(a)s"'
